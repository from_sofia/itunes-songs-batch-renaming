# iTunes songs batch renaming

This is a bash script that tries to rename all files you may have copied from your iPhone,iPod or some other apple device that via iTunes has decided to rename all your files in their TAG naming convention. Unfortunately this script is nowhere near perfect and if the files have incomplete metadata they may fail to be renamed or have incomplete names. Please use this script only if you have a copy of your data.
If all metadata for a given file is present the output naming convention should be: "$artist - $song - $album.$ext"

 - As a dependency you need to donwload [mediainfo](https://mediaarea.net/en/MediaInfo) for your distro
 - Change lines 6 and 7 to reflect your needs. 
