#!/bin/bash

# allows to store strings with spaces in variables and then use mv/mkdir/rm on these
IFS='
'
source_dir="/iphone_mount/../iTunes/Music/*/*"
result="/destination_path/to/use"
for f in $source_dir ; do
	artist=$(mediainfo $f | grep "Album/Performer" | cut -c 44-) 
	song=$(mediainfo $f | grep -m 1 "Track name" | cut -c 44-)
	album=$(mediainfo $f | grep -m 1 "Album" | cut -c 44-)
	dest_name=$(printf "$artist - $song - $album")
	mv $f $dest_name
	mv $dest_name $result
done
